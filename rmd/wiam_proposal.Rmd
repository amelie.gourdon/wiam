---
title: "What is a mathematician?"
author: "F033266 - Amélie Gourdon-Kanhukamwe"
date: 'Current version: `r format(Sys.time(), "%d/%m/%Y")`'
output:
  pdf_document:
    toc: yes
    toc_depth: 3
    includes:
      in_header: header.tex
    fig_caption: yes
    extra_dependencies:
      caption:
      - labelfont = {bf}
      - textfont = {it}
      animate: null
  html_document:
    toc: yes
    toc_depth: '3'
    df_print: paged
header-includes: \usepackage{setspace}\onehalfspacing
subtitle: COP328 - Dissertation proposal
---

```{r setup, include = FALSE, warning = FALSE, message = FALSE, results = "hide"}
# just for reminder if necessary https://cran.ma.imperial.ac.uk/
# if necessary
# devtools::install_github("espanta/lubripack")
# if necessary
# install.packages("remotes")
lubripack::lubripack(
  "magrittr",
  "skimr",
  "rio",
  "here",
  "tidyverse",
  "countrycode",
  "lubridate",
  "janitor",
  "ggplot2",
  "ggthemes",
  "showtext",
  #"scales",
  "khroma",
  "extrafont",
  "polite"
)

# Export own theme
source(here::here("src", "theme_agk.R"))

sysfonts::font_add_google("Roboto", "roboto")
showtext_auto()
showtext::showtext_opts(dpi = 96)

knitr::opts_chunk$set(
  fig.path = "figures/",
  fig.dim = c(9, 7),
  fig.align = "center",
  fig.lp = NULL,
  dpi = 96,
  echo = FALSE,
  warning = FALSE,
  error = FALSE,
  message = FALSE,
  comment = "",
  results = "markup",
  cache = TRUE
)
```
\newpage
# Introduction
<!-- Need to systematize referencing -->
<!-- À la APA, integrated with literature review-->
<!-- Currently in British English; check why journal could be interested and see if American English is necessary [check in computational social sciences?-->
<!-- Average sentence length = 35 words, need to try and take down to 25 -->

In 2019, only 28% of USA full-time mathematicians were women (United States Census Bureau, 2021). In the UK, while young women perform at similar levels as young men in GCSE Maths, they become a minority in A-Level Maths students, and more so in A-Level Further Maths (Seagull, 2019). Some, such as former Harvard dean Summers (Goldenberg, 2005), have argued that this is due to innate differences. Others have counter-argued that Summers' declaration was the uninformed view of a scholar untrained in biology or psychology, and refer, for example, to Fine's (e.g., 2005) critical assessment of the academic literature claiming to show gender differences. Yet, people agree, at least at unconscious level<!-- Check-->, with gender differences in maths ability (see e.g., Nosek et al., 2009), and women report lower self-confidence in daily numeracy (Seagull, 2019).<!-- In developed version, bring Dea et al. (2018) where greater variability hypothesis is rejected-->
How might one explain, if biological foundations for gender differences in maths are at best minimal (Halpern et al., 2007), that women are under-represented in the mathematical professions? Both Fine (2005) and Halpern et al. (2007) highlight the social environment, feeding back to girls from birth, and setting them different expectations. The argument that gender differences in maths are socially driven is particularly supported by Nosek et al. (2009), who also showed that, in countries presenting a lesser belief in gender differences in maths, girls and women achieved higher in maths. While this work does not demonstrate the causality of social factors, it counters the argument that gender differences in maths are innate.

More precisely, there are, possibly, two related social mechanisms in place in women's under-representation in maths. First, stereotypes form from observations of the environment (Koenig & Eagly, 2014), including regarding gender and science (Miller et al., 2015). Consequently, the current gender imbalance in maths may lead to the belief in innate differences in maths ability. Secondly, this belief leads girls' social environment (teachers, family, friends) to signal to them that they are less able for maths; as the self-concept is floating and malleable, society's belief in gender differences in maths becomes self-fulfilling. Therefore, there are at least two leverage points to enable change: 1) through the social environment's response to girls' interest in maths; 2) through influencing gender-maths stereotype formation. Critically, stereotype formation relies on both quantitative observations of the environment and qualitative observations, matching occupational behaviours with groups (Koenig & Eagly, 2014). Therefore, while recruiting more women into the mathematical professions can influence stereotype formation, it is also important to scrutinize the content of how these professions are presented.

For example, the rate of women mathematicians in the USA is similar to the rate of women in the computer and mathematical occupations (26%; United States Census Bureau, 2021); yet, in the occupations closest to the mathematical field, the rate of women is higher (e.g., 37% of actuaries, 50% of statisticians). This is particularly striking, as it suggests that the gender-maths stereotype might have reduced influence on women's choices: women seem confident enough in their ability to choose careers where maths are obviously prominent. Nevertheless, they do not choose the specific mathematician career. It could suggest that the mathematician career stereotype presents this career as one for the most able: that is, while women may be confident enough in their ability to engage in applied mathematical careers, the mathematician career stereotype might place, even implicitly, a bar higher than their self-perception. Following Leslie et al. (2015), it may also be that the innate nature of maths abilities is more salient in the mathematician career stereotype, accurately or not. In fact, although Leslie et al. only surveyed people already engaged in academic careers, most media present the mathematician as gifted (see e.g., Mendick, 2004), even when the mathematician is a woman (see e.g., the opening scene of *Hidden figures*, Melfi, 2016). Additionally, the content of the mathematician occupational stereotype may also pertain to behaviours that do not match women's own gender stereotype. For example, it could centre the mathematician as a researcher, which could invoke career conditions that are not gender-inclusive, <!-- Need a REF on the impact of academia on families, eg later motherhood -->or it may associate the mathematician occupation with atypical social behaviours, while gendered stereotypes present &mdash; accurately or not &mdash; women as highly sociable.

One could argue that surveying maths students could provide insight in the mathematician career stereotype and how it might steer away women maths students from this specific career. However, as discussed previously, stereotypes reflect what individuals observe in their environment, and therefore would be unlikely to change until the environment itself changes. On the other hand, while the information collected from the environment includes, in the case of occupational stereotypes, people already in these occupations, stereotype content is also influenced by how these occupations are presented indirectly, for example in different media (Koenig & Eagly, 2014; Li et al., 2020). That part of the environment, unlike the rate of women mathematicians, can be changed in the short term to, in turn, influence the content of the mathematician career stereotype.
Thus, in this paper, I aim to explore how the mathematician is presented qualitatively, to inform what content the mathematician career stereotype might include. A secondary aim will be to investigate potential gender differences in these representations.

<!-- Sway -->
<!-- Stereotype threat: https://www.sciencedirect.com/science/article/pii/S0022440514000831?casa_token=q-wvhhGah8YAAAAA:C5Giz7hodPkeGxpvsFhwBZFPTILAMC_Jv2qFzz6lOLGtnKxQ4xriNoxs57ttehHffjdpoEKID8U -->
<!-- Framework? Diekman, A., & Schmader, T. (2021). Gender as Embedded Social Cognition. https://doi.org/10.31234/osf.io/wvx2s -->
<!-- Likely to discuss: Schwemmer, C., Knight, C., Bello-Pardo, E. D., Oklobdzija, S., Schoonvelde, M., & Lockhart, J. W. (2020). Diagnosing Gender Bias in Image Recognition Systems. Socius, 6, 2378023120967171. -->
<!-- Defo to use: Penner, A. M., & Willer, R. (2019). Men’s Overpersistence and the Gender Gap in Science and Mathematics. Socius, 5, 2378023118821836. => overpersistence partly fed by the gender-maths stereotype -->
<!-- Full version, mention the official definition of the mathematician occupation by the U.S. Bureau of Labor Statistics -- centred on conducting research and solving problems problems using maths; https://www.bls.gov/soc/2010/ https://www.bls.gov/soc/soc_2010_definitions.pdf -->
<!--In full version: Field medals number, tenure, vocational level etc-->
<!-- Full version: add the leaky pipeline -->
<!-- Bring in computational social sciences, open to other discipline in full version -->
<!-- https://peerj.com/articles/cs-295/ -->

# Method
## Research philosophy

For this research project, I will purposely situate myself as a multidisciplinary researcher, building on my expertise in behavioural sciences and enriching it with data science tools to complete its aims. This is in line with my research philosophy, pragmatism (Creswell & Creswell, 2018), whereby a methodological approach is chosen because it fulfils the purpose of a project. The pragmatic approach recognizes that research is always contextualized, so I will adopt the uncommon step in computer sciences &mdash; but following Ricker (2017) &mdash; to position myself in relation to the topic, when exploring the limitations. For example, I plan to situate the project within data feminism (D'Ignazio & Klein, 2019): <!--For example, it will meet the principles of considering context, and of elevating emotion and embodiment. -->indeed, through describing the qualitative presentation of the mathematician career, and its potential gender moderation, the project will examine power, as per the first principle of data feminism, and as a point of entry in challenging power.
<!-- Intro is very post-positivist, while this is more transformative; need to work on disjointment -->

## Proposed design

I will collect secondary data from public obituaries of mathematicians published by maths societies (deliverable 1), and apply text mining techniques to analyse this data (deliverable 2). Secondary data from public obituaries limit the ethical requirements, as consent is not required, and obituaries provide a fairly consistent type of content and format over the years, allowing for some control. Additionally, Gaucher et al. (2017) suggested that stereotype content may be related to system-sanctioned ideology, which one could argue obituaries represent, at least within the scholar society publishing them. That is, obituaries may be perceived as an official voice of their publishing societies, thus having more influence on readers; identifying potential biases in their content may therefore be critical. On the other hand, it is unlikely that the maths students who have not yet chosen a maths career, and even more so the younger women who have not yet chosen a major field of studies, read scholar societies obituaries. However, women mathematicians likely do, and the gender gap persists and expands as the maths research career progresses (McWhinnie & Fox, 2013). Furthermore, biases in these obituaries would reflect the unconscious biases of these writers <!-- REF? --> who would likely communicate these implicitly when in contact with other audiences, for example when teaching students of maths and related subjects, when communicating about maths to the general public, or when interacting with colleagues. That is, these obituaries can be considered as a window into the unconscious biases of their writers, who represent key societies in the maths discipline (albeit likely not representatively - I expect that obituaries writers may turn to be, for example, more advanced in their careers).<!-- Another limitation to note will be that the obituaries will likely only be of members of the chosen societies, and therefore representativeness will be limited -->

### Deliverable 1

So that a large sample of obituaries may be collected, I will scrape obituaries from scholar societies, if and where possible. Although scraping has its own challenges, at least at legal level, it allows the collection of a large amount of data in a short time (Ignatow & Mihalcea, 2017), once scraping techniques have been learnt. However, from an exploration of the target societies' website, it is likely that a mix of scraping and manual data collection will be necessary, for example due to restrictions on scraping (see Appendix I). To gather a larger sample, data will be collected from both the American Mathematical Society (AMS) and the London Mathematical Society (LMS), while not extending to further countries to control for language differences (albeit even subtle cultural differences could be observed).
Further, to provide a comparison point, obituaries will also be collected for psychology researchers, following the same principles. That is, the American Psychological Association (APA) will be used for American psychologists, and the British Psychology Society (BPS) for British psychologists; the Association for Psychological Science (APS) could also be used as a back up or complement for American psychologists. Psychology was chosen as it is my primary discipline, and my familiarity with the field allows for additional feasibility, but it is also a research discipline which currently displays little gender gap in its number of researchers (albeit with similar issues in progression; APA, 2014).
In addition to the obituary content, I will record at least the discipline the researcher was practising (psychology or maths), minimal demographic information (gender)<!-- Limitation to consider later: gender here is likely not to capture non-binary --> about the deceased and the author of the obituary, and the date of publication (so historical trends can also be controlled for / explored). The range of dates to be included will be determined at the start of the data collection, based on availability: for example, it currently seems that the BPS has been publishing obituaries on their website only since 2014.

The corpus of obituaries that will result from this process will constitute the first deliverable of the project, and will be made openly available on at least Gitlab and the Open Science Framework (under an CC-By Attribution 4.0 International licence), two venues which are widely used in, respectively, data science and psychology. To increase the visibility, and therefore the potential impact of the corpus, it may also be deposited on the UK Data Service, and platforms identified at later stages. Finally, as part of this deliverable, the corpus will be documented following the recommendations set by Gebru et al. (2020), that is including a datasheet for the data. Unlike a codebook, a datasheet not only describes what is in the data, but also documents: the motivation for its collection; information on the data itself that could inform, in particular, the evaluation of potential bias; the data collection process; the data wrangling or other pre-processing that has taken place; what the data have been used for and potential future uses, including risks; how the data will be made available, and maintained.

### Deliverable 2

Once the data will be collected, the content of the obituaries will be analysed through text mining techniques. Text mining is well suited to address inductive research questions and, when applied with computational methods, is relevant to studying phenomenons such as stereotypes, which tend to be expressed implicitly, particularly when they are not socially desirable<!--REF-->. Traditional methods of qualitative analysis can allow such attention to implicit representations that speakers do not express, through interpretation of the themes or the narrative; however this needs done by researchers themselves, who come to the research topic with their own perceptions (Jelveh et al., 2018), and critically have limited resources, both in terms of attention and time. That is, while researchers can interpret the data and therefore go further than simply looking for associations between words and phrases, and/or their frequencies, they can only do so on a limited quantity of data, particularly when the timeframe is limited.

Computational methods for text mining instead focus on underlying patterns of association between words or phrases, so may reduce opportunities for interpretation<!-- REF -->, but as a trade-off allow to process a much larger quantity of data than human researchers could do, and in a shorter timeframe. Therefore, it allows to draw more reliable inferences from patterns in the data (albeit larger samples do not guarantee representativeness), and can even allow new insights through the increased volume of analysed data (Hagen, 2018). Further, although this project research aim is mostly inductive, I am also interested in exploring if obituaries reflect gendered representations of the mathematician career, a comparison that will be facilitated by computational text mining. Similarly, computational text mining will allow comparison of the data to pre-defined dictionaries, built based on the literature reviewed in the first phase of the project. Nevertheless, the validity of the interpretations I will draw from computational text mining could also be explored by adding a validation process against traditional text analysis methods: this could consist, for example, in drawing a random subset from the corpus, to analyse thematically prior to running the computational text mining (to allow for blind thematic analysis<!--; an alternative here would be to ask a RA-->).

Therefore, the second deliverable of this project will be a report describing the analyses discussed above, and their interpretations, situated in the wider context of the literature on stereotypes, gender gaps, maths education and maths careers, drawn without disciplinary restrictions. I aim to write this report in a publishable paper format, targeting a journal that will be chosen at the start of the project. In addition, the code developed for the analysis will be available openly on Gitlab and the Open Science Framework, along the corpus.

<!-- Pre-registration? -->
# Project plan

The timeline of the project can be seen Figure 1. There are three key categories of resources to consider for this project: the data, computational resources and time resources. All data are secondary data publicly available on the internet, so the feasibility to gather these resources only rely on computational resources and time (the latter particularly where the data cannot be scraped). In terms of computational resources, the code will rely on R, an open source software, so accessibility is not a barrier. I will work primarily using my personal computers, which are both reliable and well-resourced (16Gb of RAM and over 2 GHz CPU), and therefore should handle the scraping and the text mining without difficulty. However, if necessary, I have access to my own Higher Education Institution's IT network, and could also access the computer labs at Loughborough University. More critically, in terms of time resources, the timeline has taken into account pinch points with competing projects (on the course, but also other research projects or research service activities), and the expected high cost time for collecting the data; consequently, annual leave has been booked from my primary organization so that this project can be at the forefront once the spring term is over. My own teaching is over for the year, as well as the essential of my administrative duties, so when not on annual leave, the project will be treated as a regular research activity over working hours.

\newpage
\blandscape
```{r Gantt chart, fig.cap = "Project timeline"}
gantt_data <- data.frame(
  "event" = c("Proposal draft ", "Revise proposal", "COP528 assignment", "Annual leave 1", "Draft introduction / literature review", "Developing scraping code (where applicable) / Building database", "COP511 assignment", "Annual leave 2", "Draft method", "Annual leave 3", "Mining & visualizing", "Annual leave 4", "Draft results", "Draft discussion", "Revisions"),
  "start" = as.Date(c("2021-04-25", "2021-05-04", "2021-05-06", "2021-05-10", "2021-05-15", "2021-05-24", "2021-05-29", "2021-05-31", "2021-06-14", "2021-06-17", "2021-06-28", "2021-07-05", "2021-07-12", "2021-07-19", "2021-08-16")),
  "end" = as.Date(c("2021-05-02", "2021-05-05", "2021-05-14", "2021-05-11", "2021-06-04", "2021-06-27", "2021-06-04", "2021-06-04", "2021-06-27", "2021-06-22", "2021-07-18", "2021-07-26", "2021-07-25", "2021-08-01", "2021-08-26")),
  stringsAsFactors = FALSE
  )

gantt_long <-
  pivot_longer(gantt_data,
               -c("event"),
               names_to = "date_type",
               values_to = "dates") %>%
  dplyr::mutate(event = forcats::fct_reorder(event, desc(dates)),
                dates = as.POSIXct(dates)) %>%
  dplyr::mutate(event = forcats::fct_relevel(
    event,
    "Annual leave 4",
    "Annual leave 3",
    "Annual leave 2",
    "Annual leave 1",
    after = Inf))
gantt_long %>%
  ggplot2::ggplot(mapping = aes(dates, event)) +
  geom_line(size = 8, colour = "#696969") +
  ggplot2::scale_x_datetime(date_breaks = "1 week") +
  ggplot2::labs(x = "Week starting", y = NULL) +
  theme_agk() +
  theme(
    legend.position = "none",
    axis.text.x = element_text(angle = 90),
    panel.grid.major = element_blank(),
    axis.text = element_text(size = 10)
  ) +
  ggplot2::scale_y_discrete(labels = c(
    "Revisions",
    "Draft discussion",
    "Draft results",
    "Mining & visualizing",
    "Draft method",
    "Developing scraping code (where applicable) / Building database",
    "COP511 assignment",
    "Draft introduction / literature review",
    "COP528 assignment",
    "Revise proposal",
    "Proposal draft ",
    "Annual leave",
    "Annual leave",
    "Annual leave",
    "Annual leave"
  ))
```

\elandscape
\newpage
# References
<!--APA style for now-->
American Psychological Association (2014). *How is the gender composition of faculty in graduate psychology departments changing?* https://www.apa.org/monitor/2014/10/datapoint

Creswell, J. W., & Creswell, J. D. (2017). *Research design: Qualitative, quantitative, and mixed methods approaches*. Sage Publications.

D’Ignazio, C., & Klein, L. F. (2020). *Data feminism*. MIT Press.

Fine, C. (2005). *Delusions of gender: The real science behind sex differences*. Icon Books Ltd.

Gaucher, D., Friesen, J. P., Neufeld, K. H. S., & Esses, V. M. (2018). Changes in the positivity of migrant stereotype content: How system-sanctioned pro-migrant ideology can affect public opinions of migrants. *Social Psychological and Personality Science, 9*(2), 223–233. doi: 10.1177/1948550617746463

Gebru, T., Morgenstern, J., Vecchione, B., Vaughan, J. W., Wallach, H., Iii, H. D., & Crawford, K. (2018). Datasheets for datasets. *arXiv*. http://arxiv.org/abs/1803.09010

Goldenberg, S. (2005, January 18). Why women are poor at science, by Harvard president. *The Guardian*.

Hagen, L. (2018). Content analysis of e-petitions with topic modeling: How to train and evaluate LDA models? *Information Processing & Management, 54*(6), 1292–1307. doi: 10.1016/j.ipm.2018.05.006

Halpern, D. F., Benbow, C. P., Geary, D. C., Gur, R. C., Hyde, J. S., & Gernsbacher, M. A. (2007). The science of sex differences in science and mathematics. *Psychological Science in the Public Interest, 8*(1), 1–51. doi: 10.1111/j.1529-1006.2007.00032.x

Ignatow, G., & Mihalcea, R. (2017). *An introduction to text mining: Research design, data collection, and analysis*. Sage Publications.

Jelveh, Z., Kogut, B., & Naidu, S. (2018). Political language in economics. *SSRN*. doi: 10.2139/ssrn.2535453

Koenig, A. M., & Eagly, A. H. (2014). Evidence for the social role theory of stereotype content: Observations of groups’ roles shape stereotypes. *Journal of Personality and Social Psychology, 107*(3), 371–392. doi: 10.1037/a0037215

Leslie, S.-J., Cimpian, A., Meyer, M., & Freeland, E. (2015). Expectations of brilliance underlie gender distributions across academic disciplines. *Science, 347*(6219), 262–265. doi: 10.1126/science.1261375

Li, Y., Liu, B., Zhang, R., & Huan, T.-C. (2020). News information and tour guide occupational stigma: Insights from the stereotype content model. *Tourism Management Perspectives, 35*, 100711. doi: 10.1016/j.tmp.2020.100711

McWhinnie, S., & Fox, C. (2013). *Advancing women in mathematics: good practice in UK university departments*. Oxford Research and Policy.

Melfi, T. (Director). (2016). *Hidden figures* [Film]. Fox 2000 Pictures, Chernin Entertainment, Levantine Films.

Mendick, H. (2004). A mathematician goes to the movies. *Proceedings of the British Society for Research into Learning Mathematics, 24*(1), 43–48.

Miller, D. I., Eagly, A. H., & Linn, M. C. (2015). Women’s representation in science predicts national gender-science stereotypes: Evidence from 66 nations. *Journal of Educational Psychology, 107*(3), 631–644. doi: 10.1037/edu0000005

Nosek, B. A., Smyth, F. L., Sriram, N., Lindner, N. M., Devos, T., Ayala, A., et al. (2009). National differences in gender-science stereotypes predict national sex differences in science and math achievement. *Proceedings of the National Academy of Sciences, 106*(26), 10593–10597. doi: 10.1073/pnas.0809921106

<!--Penner, A. M., & Willer, R. (2019). Men’s overpersistence and the gender gap in science and mathematics. *Socius, 5*, 2378023118821836. doi: 10.1177/2378023118821836-->

<!--Reuben, E., Sapienza, P., & Zingales, L. (2014). How stereotypes impair women’s careers in science. *Proceedings of the National Academy of Sciences, 111*(12), 4403–4408. doi: 10.1073/pnas.1314788111-->

Ricker, B. (2018). Reflexivity, positionality and rigor in the context of big data research. In J. Thatcher, J. Eckert, & A. Shears (Eds.), *Thinking big data in geography. New regimes, new research* (pp. 70-87). University of Nebraska Press.

Seagull, B. (2019, May 19). Women and maths — what’s not adding up? *Financial Times*.

<!--Stella, M. (2020). Text-mining forma mentis networks reconstruct public perception of the STEM gender gap in social media. *PeerJ. Computer Science, 6*, e295.-->

United States Census Bureau (2021). *Full-Time, Year-Round Workers and Median Earnings: 2000 and 2013-2019*. https://www.census.gov/data/tables/time-series/demo/industry-occupation/median-earnings.html
\newpage

# Appendix I - Preliminary check of the scraping restrictions on the target websites
```{r, echo = TRUE}
bps_url <- "https://thepsychologist.bps.org.uk/obituaries"
bps_session <- polite::bow(bps_url,
               user_agent = "Amélie's MSc project")
bps_session

aps_url <- "https://www.psychologicalscience.org/tag/obituary"
aps_session <- polite::bow(aps_url,
               user_agent = "Amélie's MSc project")
aps_session

lms_url <- "https://londmathsoc.onlinelibrary.wiley.com/action/doSearch?AllField=obituary&SeriesKey=14692120&sortBy=Earliest&startPage=0"
lms_session <- polite::bow(lms_url,
               user_agent = "Amélie's MSc project")
lms_session

ams_url <- "https://www.ams.org/publicoutreach/in-memory/in-memory"
ams_session <- polite::bow(ams_url,
               user_agent = "Amélie's MSc project")
ams_session
```
