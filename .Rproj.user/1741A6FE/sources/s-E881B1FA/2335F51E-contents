---
title: "What are mathematicians and psychologists?"
author: "Amélie Gourdon-Kanhukamwe"
date: 'Current version: `r format(Sys.time(), "%d/%m/%Y")`'
output:
  pdf_document:
    toc: yes
    toc_depth: 3
    includes:
      in_header: header.tex
    fig_caption: yes
    extra_dependencies:
      caption:
      - labelfont = {bf}
      - textfont = {it}
      animate: null
  word_document:
    toc: yes
    toc_depth: '3'
  html_document:
    toc: yes
    toc_depth: '3'
    df_print: paged
header-includes: \usepackage{setspace}\onehalfspacing
subtitle: A text mining approach to accessing gender stereotype content
---

```{r Setup, include = FALSE, warning = FALSE, message = FALSE, results = "hide"}
# just for reference if necessary https://cran.ma.imperial.ac.uk/
# if necessary
# install.packages("remotes")
# remotes::install_github("espanta/lubripack")
# remotes::install_github("collectivemedia/tictoc")

# This option will allow to not run out of memory for the heavy PDF extraction
# tasks. The three lines above allows to adapt this to the machine this is
# run with.
gc()
ram <- memuse::Sys.meminfo()
ram_mb <- round(ram$totalram@size * 1024, 0)
java_param <- paste("-Xmx", ram_mb, "m", sep = "")
options(java.parameters = java_param)
rm(ram, ram_mb, java_param)

lubripack::lubripack(
  "magrittr",
  "skimr",
  "rio",
  "here",
  "tidyverse",
  "tidyfst",
  "ggplot2",
  "ggthemes",
  "showtext",
  "khroma",
  "extrafont",
  "kableExtra",
  "Superpower"
)

# Export own theme
source(here::here("scr", "theme_agk.R"))

sysfonts::font_add_google("Roboto", "roboto")
showtext_auto()
showtext::showtext_opts(dpi = 96)

knitr::opts_chunk$set(
  echo = FALSE,
  error = FALSE,
  fig.align = "center",
  fig.dim = c(9, 7),
  fig.lp = NULL,
  fig.path = "figures/",
  message = FALSE,
  warning = FALSE,
  cache = TRUE,
  comment = "",
  dpi = 96,
  results = "markup",
  tidy = "styler"
)
```

```{r Sample reduction based on length, eval = TRUE}
# Importing the data
full_wiam <- rio::import(here::here("data", "wiam.csv")) %>%
  dplyr::select(-c("V1"))

# Gettting the pre-exclusion cell sizes
gender_by_disc_count_full <- full_wiam %>%
  dplyr::group_by(assumed_gender, acad_disc, society) %>%
  dplyr::count()

# Examining the descriptive statistics for text length, with particular
# attention to differences between societies and to the AMS, where we know that
# some of the obituaries are more akin to announcements.
# There isn't a base function for the mode however, so we used this, based on
# https://www.tutorialspoint.com/r/r_mean_median_mode.htm#:~:text=R%20does%20not%20have%20a,the%20mode%20value%20as%20output.
desc_mode <- function(x) {
   uniq_vector <- unique(x)
   uniq_vector[which.max(tabulate(match(x, uniq_vector)))]
}

length_desc <- full_wiam %>%
  dplyr::group_by(society) %>%
  dplyr::filter(txt_len_s > 0) %>% # There are a few empty obituaries due to
                                   # likely irregularity in the text leading
                                   # to web scraping failing to capture all
                                   # the obituary. Thus they need to be
                                   # excluded.
  dplyr::summarise(
    median_n_words = median(txt_len_w),
    median_n_sentences = median(txt_len_s),
    mean_n_words = mean(txt_len_w),
    mean_n_sentences = mean(txt_len_s),
    shortest = min(txt_len_s),
    longest = max(txt_len_s),
    mode = desc_mode(txt_len_s),
    n = n())

# Also inspecting the length differences by looking at the distributions
length_by_soc_hist <- full_wiam %>% ggplot2::ggplot(aes(x = txt_len_s)) +
  ggplot2::geom_histogram() +
  ggplot2::facet_grid(cols = vars(society))

# The median for AMS is particularly enlightening as it suggests half the data
# set is shorter than or as long as 6 sentences.
# Thus, inspecting the new sample sizes if we exclude obituaries shorter than 6
# sentences.
gender_by_disc_count_6_sentences <- full_wiam %>%
  dplyr::filter(txt_len_s > 5) %>%
  dplyr::group_by(assumed_gender, acad_disc, society) %>%
  dplyr::count()

# And if we exclude obituaries shorter than 7 sentences.
gender_by_disc_count_7_sentences <- full_wiam %>%
  dplyr::filter(txt_len_s > 6) %>%
  dplyr::group_by(assumed_gender, acad_disc, society) %>%
  dplyr::count()

# As excluding texts shorter than 7 sentences led to a strong loss in women's
# obituaries from the AMS, we decided to fix the limit at texts longer than 5
# sentences.

# Thus
reduced_wiam <- full_wiam %>%
  dplyr::filter(txt_len_s > 5)
society_count <- reduced_wiam %>%
  dplyr::group_by(society) %>%
  dplyr::count()

# The below provide the cell ns for the power analyses.
gender_by_disc_count <- reduced_wiam %>%
  dplyr::group_by(assumed_gender, acad_disc) %>%
  dplyr::count()

# ns for cells needs to be as follows:
# n = c(woman psych, woman maths, man psych, man maths)
# Thus, dynamic assignment of ns to cells is as below
n_config <- c(
  gender_by_disc_count[4, 3], # woman psych
  gender_by_disc_count[3, 3], # woman maths
  gender_by_disc_count[2, 3], # man psych
  gender_by_disc_count[1, 3]) %>% # man maths
  unlist()
# To double-check the order visually
n_config
```

\newpage
\blandscape
```{r, eval = TRUE}
length_desc %>%
  kableExtra::kbl(caption = "Descriptive statistics for obituary length") %>%
  kableExtra::kable_classic(full_width = F)
gender_by_disc_count_full %>%
  kableExtra::kbl(caption = "Cell sizes per assumed gender, discipline and society, before excluding obituaries shorter than six sentences") %>%
  kableExtra::kable_classic(full_width = F)
gender_by_disc_count_6_sentences %>%
  kableExtra::kbl(caption = "Cell sizes per assumed gender, discipline and society, after excluding obituaries shorter than six sentences") %>%
  kableExtra::kable_classic(full_width = F)
```
\elandscape
\newpage

```{r Power analyses, eval = FALSE}
# Power analysis for null effects, to assess the type I error rate
# Each simulation is quite long to run (about 35-40 minutes *each*), so it was
# saved to a R object to be accessed easily without running it again.
# The different variance values were chosen to reflect that the smaller groups
# would have larger variances.
uneven_no_effect <-
  Superpower::ANOVA_design(
    design = "2b*2b",
    n = n_config,
    mu = c(0.6, 0.6, 0.6, 0.6),
    sd = c(1, 2, .5, 1),
    labelnames = c("gender", "woman", "man", "discipline", "psych", "maths")
  )
no_effect_pw <- Superpower::ANOVA_power(
  uneven_no_effect,
  nsims = 100000)
saveRDS(no_effect_pw, here::here("data", "no_effect_type1_rate.RDS"))
# As the type I error rate is too high, we also ran the simulation again with a
# lower alpha level (.001).
no_effect_pw_low_a <- Superpower::ANOVA_power(
  uneven_no_effect,
  alpha_level = .001,
  p_adjust = "holm",
  nsims = 100000,
  seed = 2021)
saveRDS(no_effect_pw_low_a, here::here("data", "no_effect_type1_rate_low_a.RDS"))
# For information, simulation output object reads as below.
# sim_name$sim_data # Returns each simulation's results
# sim_name$main_results # Returns power for each effect and effect size
# sim_name$pc_results # Returns power and effect size for each pairwise
# comparisons results
# sim_name$plot1 # Returns p distribution  for each effect
# (should be uniform for null effects)
# sim_name$plot2 # Returns p distribution  for each pairwise comparison
# (should be uniform for null effects)
# Thus for this specific effect, the most relevant to look at is:
type1_rates <- no_effect_pw$main_results
type1_rates_low_a <- no_effect_pw_low_a$main_results

# Then we also did a simulation-based analysis for a small effect of gender
# with no effect of discipline or no interaction, which is our central
# hypothesis. The means and the standard deviation are set to replicate a small
# effect size (which is believed to be the mean effect size in psychology
# (e.g., Richard et al., 2003), and is also more conservative.
# This also runs in about 35-40 minutes.
uneven_gd_main_effect <-
  Superpower::ANOVA_design(
    design = "2b*2b",
    n = n_config,
    mu = c(0, 0, 2, 2),
    sd = 2,
    labelnames = c("gender", "woman", "man", "discipline", "psych", "maths")
  )
gd_main_effect_pw <- Superpower::ANOVA_power(
  uneven_gd_main_effect,
  nsims = 100000,
  p_adjust = "holm",
  seed = 2021)
saveRDS(gd_main_effect_pw, here::here("data", "gd_main_effect_pw.RDS"))
```

```{r Calling the saved power analyses outputs, eval = TRUE}
no_effect_pw <- readRDS(here::here("data", "no_effect_type1_rate.RDS"))

no_effect_pw_low_a <- readRDS(here::here("data", "no_effect_type1_rate_low_a.RDS"))

gd_main_effect_pw <- readRDS(here::here("data", "gd_main_effect_pw.RDS"))
```

```{r Gender effect power analysis plot, fig.cap = "Distribution of the simulated p-values for a main effect uniquely of gender"}
gd_main_effect_pw$plot1
```
