Professor Maxwell O. Reade, who was
elected a member of the London Mathematical Society on 18 March 1948, died on 13 April
2016, aged 100 years and two days. Professor
Reade was a Professor of Mathematics at
the University of Michigan for 40 years, until
he retired in 1986, specializing in Complex
Analysis, publishing 83 papers. Professor
Maxwell was the fifth longstanding LMS
member
