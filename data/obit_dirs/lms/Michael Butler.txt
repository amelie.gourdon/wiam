Dr Michael Charles
Richard Butler,
who was elected
a member of the
LMS on 15 December 1955, died
on 18 December
2012, aged 83.
Peter Giblin writes (with advice from
Mary Rees and Claus Ringel): Michael and
his wife Sheila Brenner, who died in 2002,
were active members of the mathematics
departments of the University of Liverpool from the time of their appointments
in 1957, when they both moved from the
University of London. Until the merger of
the two departments (and Statistics) in
the 1990s Sheila was in the Department
of Applied Mathematics and Michael in
the Department of Pure Mathematics; but
from the early 1960s, and a joint research
leave to Michael’s home country of Australia, they worked together on problems
in algebra.
Michael’s earlier work was devoted to
questions in homological algebra. His
detailed study of a class of torsion-free
groups of finite rank (now called Butler
groups) showed the complexity of such
groups. His use of representations of posets in order to study abelian groups was
very influential as one of the first general
reduction techniques. In several papers he
described the surprising dichotomy between tame and wild behaviour of module categories. In their joint work Michael
and Sheila developed ‘tilting theory’, now
an indispensable tool in algebra and geometry providing a general framework for
dealing with equivalences of triangulated
categories. Their first major publication
on this was in 1980: Generalizations of
the Bernstein-Gelfand-Ponomarev reflection functors, in the proceedings of the
second ICRA (International Conference on
Representations of Algebras). From its beginning, Michael was one of the scientific
advisors for the ICRA conference series
which started in 1974 in Ottawa, Canada,
and now is held every second year in different countries. Michael and Sheila’s last
joint publication was in 2007, five years
after Sheila’s death. Together, they organized a very successful symposium at the
University of Durham in 1985.
Michael was a highly successful Head of
the (then) Department of Pure Mathematics in Liverpool in the 1980's: perhaps surprisingly so, given his strong, forthrightly
expressed, and even unfashionable, leftwing views, which were also an important
part of his partnership with Sheila. But
he also had exceptional organisational
ability, and was naturally kind, courteous, pragmatic and level headed. Michael
formally retired in 1996 but continued active in work and conference attendance
until his medical condition prevented it.
Michael and Sheila had no children, but
Michael, from a large family, has dozens
of collateral descendants, and will also
be missed by his many friends around the
world.
