Professor Phil Dyke,
who was elected a
member of the London
Mathematical Society on
28 February 2003, died
on 4 June 2020 aged 71.
Colin Christopher writes:
Phil died after a short
battle with pancreatic
cancer, diagnosed only months before. His nal days
were typically marked with more concern for his
family and his students than for himself. Throughout
his career, he thoroughly enjoyed the roles of lecturer,
head of school and author and will be greatly missed
by his colleagues and students.
Phil was born in Muswell Hill in London in quite humble
circumstances. He attended the local Secondary Modern
school where his ability in mathematics was first noticed.
After encouragement from his brother, he stayed on to
study A levels and then attended Northern Polytechnic
in 1966. This was followed by a PhD in Meteorology at
Reading University, where he met his wife Heather.
After working as a post-doc at UEA, Phil obtained his first
lectureship in 1974 at Heriot Watt University. He moved to
Plymouth in 1984, becoming Head of Mathematics in 1985.
During his time as head, Phil was always distinguished
by a genuine openness and friendship to all around
him. Even in quite trying times, he would always see
the best side of people. One striking aspect of this
people-centredness was the ability to recall the date of
birth of all of his staff–and of many others too! As a new
lecturer, I can remember well his willingness to make
time for junior staff.
Phil was promoted to a Chair of Applied Mathematics in
1993. Publishing work in geophysical fluids, he also took
a strong interest in other areas of mathematics. He had
a special love for classical mechanics and would often
burst through an office door to share his joy in some
intricate mechanics problem which he had found. This
interest, and his love of teaching, was expressed by a
growing number of undergraduate textbooks which he
authored. The last of these was occupying him during
his final illness. Phil became head of the newly merged
School of Computing, Communications and Electronics
from 2003 to 2007, where his tact and diplomacy was
put to great use in the initial years of the merger. In
2014, Phil moved to a part-time role in order to spend
time caring for his wife Heather, who had developed
early-onset dementia. His deportment over this trying
time was exemplary and earnt the respect of all his
colleagues. Heather died in 2019, and not long after this
Phil was diagnosed with cancer.
Phil was very much a family man with a great concern
and love for his children. He was also a keen musician,
playing guitar in several Jazz bands in his earlier years.
From 2001 to 2007 he was Chair of Governors for one
of the local grammar schools, a role which enabled
him to express his concern for the development of
young people in a practical way. He leaves his three
children: Ottilie, Adrian and Eleanor, and a legacy of much
kindness and thoughtfulness.
