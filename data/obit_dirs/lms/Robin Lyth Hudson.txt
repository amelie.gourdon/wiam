Professor Robin Lyth
Hudson, who was
elected a member of the
London Mathematical
Society on 17 October
1974, died on 12 January
2021, aged 80.
Dave Applebaum and
Martin Lindsay write:
Born in Aberdeen on 4 May 1940, Robin grew up in
Kidderminster, Stourbridge and Disley. He was an
undergraduate at Oxford, contemporary with David
Elworthy and Aubrey Truman (subsequent life-long
friends), and stayed on for a DPhil under John T.
Lewis’ guidance. Robin was appointed assistant
lecturer at the University of Nottingham in 1964,
promoted to a chair in 1985 and served as Head of
Department from 1987 to 1990. He spent sabbatical
semesters in Heidelberg (1978), Austin Texas (1983)
and Boulder Colorado (1996). After taking early
retirement in 1997, he held part-time research
posts at Nottingham Trent University (1997–2005),
the Slovak Academy of Sciences (1997–2000) and
Loughborough University (2005–21), and a visiting
professorship at the University of Łód (2002)
which awarded him an honorary doctorate in 2013.
Robin had 14 PhD students, including the two of
us, and has a growing number of descendants;
several postdocs and other research students also
benetted immensely from his mentorship. Starting
when they were students, and for 20 years thereafter,
Robin and his Russian-speaking wife Olga translated
articles for Russian Mathematical Surveys.
Robin is best known for his highly creative
contributions to mathematical physics and quantum
probability. An early result, now known as Hudson’s
theorem in quantum optics, shows that the pure
quantum states with positive Wigner function are
the Gaussian ones. Together with PhD students,
Robin established one of the rst quantum central
limit theorems, proved an early quantum de Finetti
theorem, and introduced quantum Brownian motion
as a non-commuting pair of families of unbounded
operators, using the formalism of quantum eld
theory. From the early 1980s he developed a very
fruitful collaboration with K.R. Parthasarathy of
the Indian Statistical Institute (Delhi Centre). They
established a (bosonic) quantum stochastic calculus
of creation, preservation and annihilation processes
that simultaneously generalises the Itô calculus for
Brownian motion and that of the Poisson process,
and incorporates a fermionic stochastic calculus
too. The resulting theory provided new insights
into the quantum theory of irreversible processes,
and signicant applications in quantum optics and
quantum control theory. It has also led to a wealth
of theoretical developments, such as quantum
stochastic ows, introduced by Robin himself, and
quantum Lévy processes. Much later, he developed a
novel theory of quantum stochastic double product
integrals which, within his extensive collaboration
with Sylvia Pulmannová from Bratislava, he related to
quantum Yang-Baxter equations and the quantisation
of Lie bialgebras. In his most recent papers Robin
also explored their implications for quantum Lévy
area.
Robin was passionate in his love of mathematics; an
inspiring teacher and a dedicated PhD supervisor. He
loved hillwalking and music, performing Bach piano
fugues himself to accompany both his inaugural
lecture in Nottingham and a public lecture Rules of
the Glass Bead Game: Reections on Mathematics
and Music in Greifswald (2003). Robin also sang
bass in the choral society in Southwell, where he
moved with his family in 1977. His gentle personality
and generous spirit endeared him to colleagues and
friends worldwide, many of whom were welcomed to
Robin and Olga’s home. He will be fondly remembered
and sorely missed.
Robin is survived by his wife Olga and children Dan,
Hugh, Lucy and Michael.
