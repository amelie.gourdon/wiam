Professor Paul Busch,
who was elected a
member of the London Mathematical
Society on 4 March
2000, died on 9 June
2018, aged 63.
Stefan Weigert writes:
Born in Refrath near
Cologne on 15 February 1955, Paul grew up as the eldest of six brothers.
He studied physics, mathematics, chemistry and philosophy at the university of Cologne. In 1982, he
obtained a PhD with a thesis supervised by Professor
Peter Mittelstaedt, a leading gure in the foundations
of physics. Paul’s Habilitation (1988), at the time a
necessary qualication for an academic career in
Germany, was followed by his appointment to Extraordinarius in 1995. In the same year he joined
the Department of Mathematics at the University of
Hull as a Lecturer and was promoted to Professor of
Mathematical Physics eight years later. As Head of
Department he was forced to oversee the sudden closure of the department, which aected him strongly.
From 2005 onward he held a professorship at the
University of York and, over time, became a member
of learned societies such as the Institute of Physics
and the Académie Internationale de Philosophie des
Sciences.
The early exposure to conceptual problems of quantum theory posed by uncertainty relations, quantum
measurements and incompatible observables turned
out to be formative for Paul’s scientic interests. For
more than three decades he sought to clarify conceptual muddles besetting the foundations of quantum
theory, by providing mathematically rigorous answers.
A well-developed physical intuition guided his eorts,
combined with the willingness to think long and hard
about dicult questions. Early on, he advocated the
use of positive-operator-valued measures (POVMs),
which represent the most general type of quantum
measurements; they have now become an indispensable tool in quantum information theory.
Nearly 100 refereed papers vividly demonstrate Paul’s
rare gift of physical insight combined with mathematical rigour. He co-authored three widely-read
books (1991, 1995, 2016) on foundational aspects of
quantum theory; all of them are still in print. His
meticulous and critical — but impartial — attitude
was sought after by journal editors and colleagues
alike. Paul’s large number of collaborators and friends
included ten PhD students who beneted from his
nearly unlimited availability, his persistent questioning, curiosity and generosity to share what he knew.
Paul’s background
proved to be important for his recent
and widely-noted work
on quantum measurement uncertainty relations (2013, jointly
with long-time collaborators P. Lahti
and R.F. Werner).
The physics underlying Heisenberg’s famous “microscopeargument”, which he
used in 1927 to motivate the incompatibility of an
electron’s position and momentum, is reassessed
and put on rm grounds. To further investigate
the fundamental limits of high precision quantum
measurements, the Royal Society awarded Paul a
Leverhulme Trust Senior Research Fellowship (2017–
18).
Paul was a sociable person, and his open mind aligned
naturally with an open house inviting colleagues and
students on many occasions. He played the piano
well, and would have liked to play more often.
Rational and disinterested argument — in the positive sense of the enlightenment — was close to
Paul’s heart. He despaired at political games played
with(in) universities, at dishonest political discourse
and at needless human misery.
Paul was a gentle man, and a gentleman. I like to think
of him as an enlightened scholar from the 18th century, transposed into today’s world. In his modesty,
Paul would probably intervene at this point, kindly
telling me not to exaggerate.
