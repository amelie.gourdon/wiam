Professor James B. Riles, who was elected a
member of the London Mathematical Society
on 15 November 1962, died on 30 November
2015, aged 77.
Christine Stevens and Russell Blyth write:
James B. Riles completed the PhD in 1967 at
Queen Mary College, University of London,
under the direction of Karl Gruenberg and
Kurt Hirsch. In the same year, he joined
the mathematics department of Saint Louis
University in the USA, where he served as a
professor and administrator for more than
three decades, attaining the rank of Professor
of Mathematics and Computer Science in 1976.
He spent sabbatical leaves at Queen Mary
College (1975) and Cambridge University
(1983 and 1990). Jim was the adviser of three
PhD students: Richard Scherer, Mohammad
Azarian and Mark Hopfinger.
Jim's mathematical interests focused on the
study of infinite groups, to which he contributed the notions of nearly maximal subgroups,
near and non-near generators, and thence
of near Frattini subgroups. He also introduced the notion of a near complement of a
normal subgroup of a group. He developed
these concepts in a pair of papers, the second
of which was published in the Journal of
the London Mathematical Society. His work
was subsequently used by Azarian and
R.B.J.T. Allenby, among others, to establish
results about generalized amalgamated free
products of groups. An enthusiastic lecturer,
Jim was always eager to learn new mathematics, and his carefully prepared seminar talks
were models of precision in which the completion of a tricky proof was often punctuated
with a twinkle in his eye.
Although mathematics was Jim's passion, he
was deeply interested in music, as well. He had
played the French horn in the All Southern California Junior High School Orchestra and could
always be counted upon for a knowledgeable
and enlightening commentary on any concert
that he attended. Jim was also a keen mountaineer who enjoyed climbing, without special
equipment, some of the highest peaks in
Rocky Mountain National Park.
Jim is survived by his wife, Linda, their three
children, and six grandchildren.
