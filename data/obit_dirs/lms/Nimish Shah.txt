Mr Nimish Shah, who was elected a member of
the London Mathematical Society on 23 June
2000, died on 16 November 2011, aged 45.
Simon O’Keefe writes: I met Nimish Shah
when he came to York in 2006 hoping to
gain a PhD in Computer Science. I became his
supervisor for his studies in computer science,
but I think it is true to say that his real interests
lay in mathematics, and in particular the mathematics used to describe computation. It was
clear from the start that Nimish was a very intelligent person, amiable and eager to do well in
all spheres of intellectual pursuit. He had very
clear ideas about what he wanted to achieve,
and once started down a particular track he
was not easily deflected from his chosen course.
He had flashes of brilliance and certainly had
the potential to complete a PhD, but unfortunately a recurrence of health problems that had
dogged him for some years meant that he had
to interrupt his work a number of times. The
productive work on his thesis slowed, and he
died whilst writing up. Although his research
output was not great in volume, what he did
publish will continue to have an influence on
other researchers in the field.
