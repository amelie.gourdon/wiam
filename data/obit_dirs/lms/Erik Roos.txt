5 f Member Jan-Erik Roos: 1935 - 2017 Professor Jan-Erik Roos who was elected a member of the London Mathematical Society on 17 1980 died on 15 December 2017 aged 82.
Clas L�fwall (University of Stockholm) writes: JanErik Roos died in his home in Uppsala Sweden.
He was born in Halmstad and he started his mathematical career as a student of Lars G�rding in Lund.
He presented his licentiate thesis 1957 about ordinary differential equations.
After a period in Paris during which he met all the leaders in algebra at that time he returned to Sweden where he started to build a school in algebra.
He became professor in Stockholm in 1970 without a doctor's degree; there had never been time for him to get a doctorate!
With enthusiasm and openmindedness he created a warm atmosphere in the department that through the years attracted many guest researchers and doctoral students.
I also came from Lund to Stockholm in 1970 as a student and I had Jan-Erik as my supervisor.
I produced a formula for the Poincar� series of a local ring (Rm) with m3 = 0.
Seeing this Jan-Erik was able to prove an analogous result for CW-complexes of dimension at most four about which he got in contact with a research group in rational homotopy theory.
An intense period of cooperation began which culminated with the conference Algebra Algebraic Topology and their Interactions in Stockholm 1983.
A major contribution of Jan-Erik's career was bringing together the two research fields of local algebra and rational homotopy.
Jan-Erik started his career in the field of local noetherian categories and he ended up an expert in experimental mathematics.
He was able to find algebraic objects with 'strange' behaviour through computer experiments but was not always so interested in proving his findings!
I had the privilege of collaborating with him a couple of times searching for proofs.
One particularly memorable occasion was when our friend David Anick had offered to buy dinner at the Operak�llaren restaurant for anyone who could prove a problem he set up about a Lie algebra with quadratic relations and bounded growth.
When I saw that Jan-Erik had found such an example experimentally I offered him the dinner if he could present the proof.
We spent a long time working together and at last we could invite each other to the restaurant write to Anick and share the bill!
Jan-Erik was always working on a new article and he produced a lot many of them as Comptes Rendus notes.
His last article he sent to arXiv in spring 2017.
Even if he spent most of his time doing mathematics his family was always his first priority.
He married Karin in the early seventies and they had a daughter Sara.
Karin also brought to the family two daughters from a previous marriage Anna and Eva.
Jan-Erik and Karin were always very welcoming; guests at the department were always welcome to visit them in their big house in Uppsala even if it was not so easy to find a free place among all the books and papers!
