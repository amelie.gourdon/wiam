Professor Fred H.J.
Cornish, who was
elected a member of the
London Mathematical
Society on 19 October
1990, died on 16 May
2020, aged 89.
Tony Sudbery writes:
Fred was born on
29 June 1930 in Exeter and attended Queen
Elizabeth’s Grammar School, Crediton. He was an
undergraduate at Wadham College, Oxford, and
while an undergraduate he co-authored three papers
on conductivity in metals. He then completed a
DPhil in general relativity. He held a postdoctoral
position in the University of British Columbia at
Vancouver, where he met his wife Monica, and then
served a spell in the Navy. From 1959 he was a
lecturer in mathematics at the University of Leeds
until 1967, when he was appointed Professor and
Head of Department of Mathematics at the recently
founded University of York, following the tragic death
of Paddy Kennedy, its rst head of department.
He was Deputy Vice-Chancellor of the University
between 1975 and 1981; this included a period as
Acting Vice-Chancellor in 1978.
Fred was active and widely respected in university
administration and in national policy-making for
university mathematics. In the 1990s he was chair of the
committee that led to the “Neumann report” of the LMS,
which was responsible for the introduction of 4-year
undergraduate degrees in mathematics (the MMath). His
research activity continued in both general relativity
and classical electromagnetism, following his work at
Leeds on the definition of energy and momentum of a
gravitating system, on incorporating electromagnetism
in general relativity, and on the interaction between
radiation and point charges in classical electromagnetism.
In 1986 he published a proof that according to classical
electromagnetism a small electric dipole can undergo
self-acceleration, an argument that aroused extended
interest. With his research students he produced an
explanation of the lack of gravitational radiation in the
“photon rocket” solution of Einstein’s equations, and
other topics in general relativity. But possibly his most
influential work lay outside this area, in his analysis
of the equivalence between the hydrogen atom and
the four-dimensional harmonic oscillator in classical and
quantum mechanics.
Fred was admired throughout the University of York for
his integrity, selflessness and sense of fair play, and for
the welcome and help that he gave to new staff. John
Brindley, a colleague in Leeds, comments that “He was
a much loved colleague and a real gentleman in every
sense of that word”. He and Monica extended great
hospitality to the mathematicians and to many others
in York. Fred was a model of academic life for younger
staff, who held him in great esteem and affection.
Monica died in 2019. They are survived by their children
Rachel, John and Richard, and by six grandchildren.
