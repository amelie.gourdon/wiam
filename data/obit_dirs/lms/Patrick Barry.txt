Professor Patrick D.
Barry, who was elected a
member of the London
Mathematical Society on
21 December 1961, died
on 2 January 2021, aged
86.
Finbarr Holland writes: At
the age of thirty in 1964
Barry was appointed Professor of Mathematics
at University College, Cork (UCC), one hundred
years after the death of George Boole, Cork’s
rst Professor of Mathematics. To mark the
centennial Cork University Press issued George
Boole: A Miscellany, a booklet of essays edited
by Barry. His appointment was followed shortly
afterwards by that of President Donal McCarthy
(1967–78), who set about modernising the College.
Barry was central in McCarthy’s ambitious plans.
Following a sabbatical at Imperial College, London,
his peers elected Barry to UCC’s Governing Body.
As UCC’s rst Vice-president (1974–76) he led the
way in streamlining the appointments system and
also helped to acquire degree-awarding status for
Limerick’s Teacher Training College.
His tenure as Head of Department (1964–99) was
one of harmony and collegiality. Even-tempered,
thoughtful and non-confrontational by nature, he
always managed to coax consensual decisions at
meetings he chaired. Having to cater for a broad
range of student ability, interest and class size, he
appointed sta with a diverse range of specialisms,
and gave each the freedom to develop his or her
own course
Towards the end of his role as HoD he introduced
an innovative part-time two-year postgraduate
degree course in Mathematical Education for
secondary-school mathematics teachers. This was
oered on only two occasions in a ten year period,
and was availed of by about forty teachers drawn
from all over Munster. It raised their prole, earned
them an extra salary increment and was hugely
benecial to their students.
Barry had a life-long passion for geometry and
loved to teach it. Following his retirement in 1999
he prepared for publication a rigorous account of
Euclid’s geometry suitable for teachers of school
Mathematics, based on Birkho’s approach. A rst
edition of his Geometry with Trigonometry was
published in 2001 and a second edition in 2015; it is
the foundation of the geometry section of the current
Mathematics syllabus for the Leaving Certicate.
Simultaneously, he wrote up notes under the heading
Generalizations of Geometry which he had almost
completed before his health began to decline. These
notes form the basis of his third book Advanced Plane
Geometry published in 2019.
Patrick Denis Barry — Paddy to everybody who knew
him — was born in Ballynacargy, Co. Westmeath,
Ireland, in 1934. His father was a Garda sergeant,
his mother a Primary School teacher. He was the
fth in a family of three boys and four girls. When
Paddy was two the family moved to Co. Cork.
There, he received his secondary education at the
Patrician Academy, Mallow, his performance in the
Leaving Certicate examination in 1952 winning him
a university scholarship from Cork County Council.
In the same year, he achieved rst place in the UCC
Entrance Scholarship examination, and second place
in the examination for the Irish Civil Service. During
his schooldays Paddy played cricket, soccer, tennis
and badminton, the latter a sport at which he was
particularly skilful, and continued to play late in life.
With two scholarships Paddy became the rst pupil
from his school to go to university, graduating in
1955 with a rst class honours BSc in Mathematics
and Mathematical Physics. That same year he
represented Ireland at badminton at Under-21 level.
He remained in UCC for a further two years
studying for his master’s degree, winning as well the
prestigious Travelling Studentship in Mathematical
Science from the National University of Ireland. He
declined this award, having already accepted the
position of Research Assistant to Walter Hayman FRS
at Imperial College, London. He earned a Diploma
from IC and a PhD from London University for a
thesis entitled On the Minimum Modulus of Integral
Functions of Small Order, based on his own solution
to his own problem, which was typical of him. Indeed,
writing in My Life and Functions, Hayman says that
“Barry was the only student I ever had who came to
me with a PhD already prepared”.
On receipt of his doctorate Paddy spent a year
at Stanford University as a Mathematics Instructor
before returning to his alma mater in 1961 to become
rst a lecturer in the Mathematics Department, and
later Professor and Head of Department in 1964,
positions he occupied until his mandatory retirement
in 1999.
Shortly after returning to Cork, Paddy met and
married Frances King, a vivacious young woman from
Belfast, whose sister’s Belfast boyfriend had a post in
UCC’s English Department, and was Paddy’s atmate!
Fran became a secondary teacher of Mathematics,
later acquiring a PhD in group theory to become one
of the few Irish secondary teachers with a doctorate.
Paddy loved cooking and liked to show o
his culinary skills at dinner parties hosted by
Fran and himself, producing a variety of exotic
dishes, made — according to his children — with
mathematical precision! His speciality was a delicious
cinnamon-infused apple pie.
Paddy died in a Dublin nursing home, from covid-19,
Fran having pre-deceased him by fteen years. They
are survived by their children: Conor, a lm maker
in Dublin, Una, who practises general medicine in
Calgary, Canada, and Brian, a surgeon in Cork.
