Slava Kurylev: 1952 - 2019 Slava Kurylev who was elected a member of the London Mathematical Society on 10 1996 died on 19 January 2019 aged 66.
Bill Lionheart writes: Kurylev was known for his work on inverse problems for partial differential equations especially for the combination of analysis and differential geometry.
Yaroslav Vadimovich Kurylev to give his full name was born in December 1952 in Kalinin in the USSR (now Tver in Russia).
He graduated from Leningrad School 239 (which specialised in Mathematics and Physics) in 1970 and went to read Physics at Leningrad State University.
He graduated in 1976 with a masters' degree in mathematical physics.
He had initially started reading just physics but he found he had no inclination towards laboratory work and so was naturally drawn to the mathematical side for which he had a gift.
He went on to study for his PhD at the Leningrad Branch of Steklov Mathematical Institute of the Academy of Science supervised by Vassili Babich.
In 1975 he defended his thesis Asymptotics of the spectral function of a second-order elliptic differential operator.
He continued to work at the Steklov Institute and then held a two year temporary position at Purdue before moving to the UK for a permanent position at Loughborough in 1995.
He made his name from the application of geometric methods to inverse problems for partial differential equations specifically methods to reconstruct a Riemannian manifold from its boundary spectral data including the "Boundary Control" method he developed with Belishev.
Soon after arriving at Loughborough he became interested in applications of inverse problems as well as more theoretical problems.
My own first contact with Slava was a long phone call in which he explained his plan for the widening application of geometric methods of inverse problems and specifically to gather people together with interests in pure and applied aspects of inverse problems in the UK from academia and from industry.
Slava was i i "" - 2019/5/7 - - - 42 rapidly promoted to a chair at Loughborough and had a wide circle of collaborators tackling problems in medical and seismic imaging.
In 2007 he moved to University College London.
The geometric theory of inverse problems gave rise to counterexamples in practice things that could not be seen.
In 2009 he wrote a paper with Greenleaf Lassas and Uhlmann entitled Cloaking devices electro-magnetic wormholes and transformation optics.
In 2018 with Lassas and Uhlmann his paper Inverse problems for Lorentzian manifolds and non-linear hyperbolic equations showed how tools developed for general relativity might be used in medical imaging specifically for elastography.
One hopes that that work might help one day in the diagnosis of cancer.
For him he already knew in 2018 that his own cancer was terminal.
He spent that time with his close family watching French comedy films enjoying tasting wine (although he could no longer drink it properly).
He continued working until the very last weeks with colleagues visiting him at home from Japan Finland Spain and the USA.
His last work included geometric tomography for connections on manifolds sometimes reducing his morphine dose so he could concentrate.
His collaborators Lauri Oksanen and Matti Lassas told me at his funeral that his last utmost desire was for this work to be finished leaving them with details of what he hoped could be achieved.
Slava is remembered fondly by all those whose life he touched and in the mathematical community especially for his encouragement and support of younger colleagues.
