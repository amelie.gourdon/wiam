Professor James
(Jim) Gourlay Clunie,
who was elected a
member of the London Mathematical
Society on 20 February 1958, died on
5 March 2013, aged
86.
Phil Rippon writes (with advice from Milne
Anderson): Jim was born on 26 October 1926 in
St Andrews and attended Madras College there,
where he was awarded the college’s prestigious
Dux (leader) in science in 1945. He completed his
undergraduate studies at St Andrews University
and then moved to Aberdeen University for his
PhD, supervised by Professor Archibald James
Macintyre. In his PhD thesis Jim developed what
quickly became the ‘modern’ approach to the
subject of Wiman-Valiron theory, published in
two papers in the Journal of the London Mathematical Society. His first lectureship was at the
University College of North Staffordshire, now
Keele University, and in 1956 he moved from
there to Imperial College London, where Professor Walter Hayman was setting up what was
to become a world-leading group in complex
analysis.
At Imperial College, Jim’s complex analysis
research flourished and he wrote many hugely
influential papers, and supervised seven research students. His papers included one on the
coefficients of univalent functions, published in
Annals of Mathematics, that led to the introduction of the so-called ‘Clunie constant’; another in
the Journal of the London Mathematical Society
that contains a result now known as ‘Clunie’s
lemma’; and a joint paper in Crelle’s Journal with
Professor J. Milne Anderson and Professor Christian Pommerenke that established the theory of
so-called Bloch functions.
Jim was promoted to a chair at Imperial College in 1964 at the young age of 39 and retired
early in 1981, moving first to a research fellowship at the Open University and then in 1986 to
a research fellowship at York University, after
which he retired properly. All his life he had
fought hard to overcome a disability caused by an attack of childhood polio, which made standing and walking increasingly difficult as the
years passed.
Jim married Nancy Tuff in 1955 and many colleagues remember their kindness and hospitality in London, Milton Keynes and York. Jim was
regarded extremely warmly by many friends,
and widely respected as a wise and knowledgeable colleague with a fine sense of humour. Indeed, his general knowledge was extraordinary,
and his ability to complete The Times crossword
before breakfast at conferences legendary. He
is remembered too with great fondness by colleagues at the constituent colleges of the National University of Ireland, where he was the
mathematics external examiner for many years
and received an honorary doctorate of science
in 1988.
Jim is survived by his daughter Fiona and
granddaughter Alex.
