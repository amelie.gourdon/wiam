Dr Anthony J.B. Potter, who was elected a
member of the London Mathematical Society
on18 January 1976, died on 3 April 2012, aged
64.
Rob Archbold writes: Tony Potter was a devoted member of the Mathematics Department
in Aberdeen from 1972 until his retirement in
2011. He was a mathematical analyst and his
research involved the application of functional
analysis and topological methods to the study
of non-linear differential equations. Particularly striking was his solo work on the application
of Hilbert’s projective metric to non-homogeneous operators. He also collaborated with a
number of leading figures in the area, spending various sabbaticals at Rutgers and Sussex.
His former colleague and collaborator, Michael
Crabb, writes ‘I owe Tony a great deal mathematically; it was his seminar, with Daciberg
Goncalves, that got me interested in Fixed
Point Theory and the Fuller index in particular’.
In his general approach to mathematics (both
teaching and research) Tony was rigorous, meticulous and highly knowledgeable. His clarity
in teaching was very much appreciated by his
students. He taught throughout the Mathematics and Engineering Mathematics curriculum, was an Advisor of Studies for many years,
a sub-warden in Johnston Hall and a dominant
force in staff-student football matches of the
1970s. Tony had a cheerful disposition and
was generous with his time, always ready to
respond to requests for help from students or
colleagues throughout the University. In recent
times, he collaborated with David Heald (Aberdeen Business School) on mathematical modelling for the UK-Scotland Barnett formula.
Outside the University, Tony was a member of the Caledonian Cricket Club for several
years. He batted with great panache rather
than as a long term investment and he enjoyed
hitting huge sixes. He was also a member of the
Golf Clubs at Braemar and Newmachar.
Sadly, Tony was able to enjoy only a brief period of retirement in Edinburgh. He is deeply
missed by his partner, Barbara, his family and
his former colleagues.
