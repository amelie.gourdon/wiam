Eric Wallace, who was
elected a member of the
London Mathematical
Society on 17 December
1964, died on 23 June
2018, aged 87.
Chris Lance writes: Eric
was the youngest of six
children of William and
Catherine Wallace in Aneld. His father was a marble
polisher and his mother was a tram conductor
until she married. Eric was always very proud of
his Liverpool roots. From Aneld Road Elementary
School he passed the 11 plus into Alsop High School,
where he was grateful for an excellent education. He
was the only member of his family to continue to
higher education, living at home while a student at
the University of Liverpool as he could not aord
accommodation elsewhere.
His mathematical talent impressed his teacher
A.G. Walker (a future LMS President, 1963–65),
who took him on as a PhD student. He gained
his doctorate in 1956 with a dissertation On the
Classication of Lie Algebras. He then, apparently
at Professor Walker’s suggestion, chose to do his
National Service in the Navy, where he achieved
the rank of lieutenant despite a lack of nautical
skills, by teaching mathematics and the art of
semaphore signalling to the naval recruits. In 1956,
while stationed in Portsmouth, he married Anita
Fisher and they set up home in the Navy’s married
quarters.
In 1958, with his National Service completed, Eric took
up a post in the University of Leeds, where he spent
his whole career as a Lecturer and later a Senior
Lecturer in the Department of Pure Mathematics until
his retirement in 1996, interspersed with sabbatical
teaching visits in New York (SUNY Bualo), Illinois and
Michigan. He published several papers on Lie algebras
and associative algebras, and served a three-year
term as Head of Department. His three PhD students
in Leeds, John Fountain, David Towers and Mike
Holcombe, all went on to successful academic
careers. He also acted as an Open University Tutor
in Pure Maths for a number of years.
Eric and Anita were the social heart of the
Department, and were largely responsible for its
warm and friendly atmosphere. In the 1960s and
1970s, when universities had many young lecturers
with growing families, Eric was among a small group
who each year organised a grand Christmas party
for the many children of the Maths and Computing
departments. Professors and parents provided funds,
and Santa Claus (possibly in the form of Eric himself)
had presents for the children. In later years Eric and
Anita continued to give many parties at their large
house in Pool-in-Wharfedale.
Anita developed severe health problems, and Eric
cared for her devotedly for several years until her
death in 2003. But he found happiness again with
Moira Wright, whom he married in 2008, and they
continued the tradition of hospitality with dinner
parties for Eric’s colleagues, most of whom had
retired by then.
In 2018, Eric had a severe stroke which left him almost
completely incapacitated. His last weeks in hospital
were dicult, but he was cheered by visits from his
old colleagues and friends. He is much missed by
Moira, his daughter Rachel and seven grandchildren.
His son Jonathan died in 2017.
