Professor Alfred Jacobus (Alf) van der Poorten
who was elected a member of the London
Mathematical Society on 5 May 975, died
on 9 October 200, aged 68.
David Hunt writes: Alf was born in Holland
in 1942 and after many difficulties his family
migrated to Sydney in 95. Alf shone academically at both Sydney Boys High School and then
as a mathematics cadet at the University of
New South Wales. He obtained four different
degrees from the university but was always destined to become an academic mathematician.
Alf’s research was in diverse aspects of
number theory. His doctoral supervisor was
Kurt Mahler, under whom he wrote a thesis
Simultaneous algebraic approximations to
functions, but in his early years he also came
under the influence of George Szekeres.
Alf collected a large network of research
colleagues. He published about 80 papers
with some fifty collaborators. Many of his publications were in some sense expository. Of particular note was his book Notes on Fermat’s Last
Theorem.
In 979 Alf was appointed to a full professorship in Mathematics at Macquarie University, which is also in Sydney. He remained in
this position until his retirement in 2002. During this period he became heavily involved
in the senior management of the University
both as Head of School and as President of the
Academic Senate. His contributions to mathematics in Australia were manifold including
chairing a working party which reported to
the Australian Research Council on mathematics research in Australia and two years as President of the Australian Mathematical Society.
In recognition of his services to mathematics in Australia, Alf was awarded the inaugural
George Szekeres Medal of the Australian
Mathematical Society in 2002. He was also appointed a Member of the Order of Australia,
AM, in 2004.
Alf was an inveterate traveller, especially to
MSRI and to Bordeaux, where he was awarded Docteur Honoris Causa in 998. But above
all else he was dedicated to his family. Alf is
survived by his mother, his wife Joy, children
David and Kate, and four grandchildren.
