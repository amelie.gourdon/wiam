Professor Nigel Kalton died on  September
2010 , aged 64.
Nigel graduated from Cambridge University
and completed his doctorate in 1970 under
the supervision of Ben Garling. After a year
in Warwick, he moved to University College
of Swansea before leaving in 99 for the
University of Missouri in Columbia, USA.
Nigel was an outstanding and highly influential mathematician. He wrote over 270 papers
and six books on diverse topics both within
Functional Analysis and in related areas of analysis. He was a renowned problem solver and
was even known to have solved one problem
of some twenty years standing during a lecture
where it was being discussed!
In 2005, he was awarded the prestigious
Banach Medal from the Polish Academy of Sciences for the most significant contributions to
Banach space theory. In Missouri, Nigel won the
Chancellor’s Award for Outstanding Research
in 1984 and the Weldon Spring Presidential
Award for outstanding research in 1987.
Nigel loved to travel and was a much
sought-after speaker at conferences. He visited many mathematics departments throughout the world for extended stays when on
study leave, his influence and enthusiasm
inspiring countless mathematicians. He was
also a popular teacher with his students,
both undergraduate and graduate, being
approachable and generous with his time.
Beyond mathematics, Nigel had many interests. He was a talented chess player, winning
the Major Open at the British Championship
in 1970, though giving up competitive play
in 1976. He was also a racquetball enthusiast.
Above all, he was a family man, never happier
than when playing with his children and grandchildren. He is survived by his wife Jennifer, children Neil and Helen, and four grandchildren.
