TIM LISTER Dr Tim Lister who was elected a member of the London Mathematical Society on 2 1986 died on 13 January 2015 aged 74.
Derek Goldrei writes: Tim was amongst the first mathematicians employed by the Open University (OU) and played a key role in shaping curriculum and the teaching of tens of thousands of students of mathematics and computing.
Tim was brought up in South Africa.
He obtained a double first in Pure Mathematics and Physics from the University of Natal in Durban in1962 and went on to postgraduate study under Professor Hanno Rund in Pretoria.
Tim moved to the UK because of the political situation in South Africa.
In 1970 he became one of the first Staff Tutors in Mathematics at the OU.
Located in the East Midlands his role was to appoint train develop and manage Associate Lecturers in mathematics and later computing.
Alongside his regional work Tim was influential in the Faculty creating and presenting courses.
Tim worked on mathematics courses ranging from the very first Foundation Course in Mathematics which started in 1971 which was studied by all new OU mathematics students through introductory pure mathematics to more advanced courses in complex analysis and mechanics.
His interest in programming and mathematical software for instance using Cabri to teach geometry culminated in his leading the production of the discrete mathematics course M263 Building Blocks of Software which supported the computing curriculum from 2004 till 2013.
By devising and chairing M263 Tim was a key influence on the teaching of more than 500 students every year.
At any one time Tim managed more than 50 Associate Lecturers having worked to establish the OU's high standards for supporting students in the early 1970s and he himself taught groups of students and at summer schools where he was an inspired teacher and mentor of others.
His teaching of mathematics and computing has been truly influential.
The OU's courses are written and maintained by teams of academics and designers and Tim was a notable on all his teams for his creativity dedication incisiveness charm warmth and wit.
In a similar manner Tim led and guided his teams of tutors who held him in the highest and fondest regard.
His great passion was for bringing an understanding of mathematics to adult students and to the founding principles of the OU which was undiminished throughout his career and beyond his retirement in 2005.
His leaves his wife Pat son TJ and daughter Tamsin who followed her father into the OU and grandchildren Tommy and Ted.
LMS NEWSLETTER http://newsletter.lms.ac.uk 
