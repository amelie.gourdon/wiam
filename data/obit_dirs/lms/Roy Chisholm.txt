Professor Roy Chisholm,
who was elected a
member of the London
Mathematical Society
on 16 May 1986, died on
10 August 2015, aged 88.
Ruth Farwell writes:
Roy was the founding
Professor of Applied
Mathematics at the University of Kent, influencing the development of applied mathematics at Kent over nearly 30 years. A highly
regarded mathematical physicist, he is remembered with much affection and respect
by those who had the good fortune to collaborate with him.
Roy studied mathematics at Christ’s
College, Cambridge, a Wrangler in part 2
and achieving a distinction in part 3 of the
tripos. In 1952 he obtained a doctorate from
Cambridge, with research into Feynman
graphs, breaking new ground with his
derivation of the ‘symmetric integration’
formula.
After Cambridge, he moved to Glasgow
University and then to University College,
Cardiff. His research into different particle interactions and their equivalencies continued.
He also published on statistical mechanics,
and co-authored a text book on mathematical methods for scientists and engineers.
Many institutions, including Texas A&M,
Stanford and CERN, welcomed Roy as a
visiting academic over the years. At CERN
in the early sixties, Roy developed the full
set of algorithms for scalar products of the
Dirac gamma algebra, to which his name is
attached – the Chisholm-Caianiello-Fubini
Identities.
Roy returned from CERN in 1963 to spend
two years in his first chair at Trinity College,
Dublin, then spending a year in the US prior
to taking up the Chair at Kent in 1966. He
built up the applied group with specialisms
in numerical analysis and numerical computations applied to physics, including Padé
approximants. A successful Summer School
which he organised at Kent in 1972, and
which brought together mathematicians
and physicists, stimulated Roy to develop
multivariate Padé approximants with which
the name of the group at Kent then became
associated.
In the mid 1970’s Roy’s research took a
new direction inspired by the algebras of
William Clifford. Roy’s and my ‘spin gauge
theories’ are Lagrangian field theories
unifying the interactions of the fundamental particles based on Clifford algebras
of higher dimensions. The most notable
achievement was predicting the mass of
the top quark which was very close to the
experimental measurement.
Roy’s positive experience of the earlier
Padé conference led him to organise an
international conference at Kent to bring
together small research groups from around
the world interested in Clifford algebras. It
triggered a series of conferences which still
continue; Roy is recognised internationally
as one of the founding fathers of mathematical applications of Clifford algebras.
For Roy, the work on Clifford algebras
opened new avenues. William Clifford, with
his wife, Lucy, was at the centre of scientific
and literary culture in London in the late
nineteenth century. With his wife, Monty,
Roy found himself moving into new aspects
of research: history and philosophy inspired
by the lives of William and Lucy.
Although Roy’s health deteriorated in
recent years, he remained active with an
enquiring mind, even publishing his first
novel, Changing Stations, in 2014. He is
survived by his wonderful wife, by his three
beloved children and his granddaughters,
of whom he was immensely proud. I and
many others feel privileged to have been
able to have him as part of our professional
lives.
More details of Roy’s career and his work
can found in his memories on www.roychisholm.com.
