Walter Craig who was elected a member of the London Mathematical Society on 17 1991 died on 18 January 2019 aged 65.
Catherine Sulem and Gene Wayne write: Born on 28 1953 in State College Pennsylvania where his father the logician William Craig was a professor in the mathematics department at Penn State Walter Craig was one of the leading analysts and applied mathematicians of his generation.
He received an A.B. and M.S. in Mathematics from UC Berkeley and a PhD under the supervision of Louis Nirenberg from the Courant Institute (NYU) in 1981.
After positions at CalTech and Stanford Walter moved to Brown i i "" - 2019/6/21 - - - 47 University in 1988.
In 2000 Walter his wife Deirdre Haskell and their daughter Zoe moved to Canada.
Walter was offered a Tier 1 Canada Research Chair in Mathematical Analysis and its Applications the very first appointment through the CRC program in the country.
Walter was renowned for his work on nonlinear partial differential equations Hamiltonian dynamical systems and their applications in particular to fluid dynamics and mathematical physics.
He had broad mathematical and scientific interests and was skillful at communicating with people from different scientific backgrounds.
His work was recognized by numerous prestigious awards including an Alfred P.
Sloan Fellowship an NSF Presidential Young Investigator Award and a Killam Research Fellowship.
He was named a Fellow of the Royal Society of Canada in 2007 Fellow of the AAAS in 2008 and was among the inaugural class of Fellows of the AMS in 2013.
In 2013 he was appointed Director of the Fields Institute.
His contributions at McMaster University were recognized with conferral of the title Distinguished University Professor in 2016.
In addition to being a brilliant researcher Walter was also an exceptional teacher and mentor who educated and supervised many undergraduate and graduate students postdoctoral fellows and young mathematicians.
Many of them have gone on to become accomplished scholars in various countries around the world.
Walter was a constant source of inspiration whose enthusiasm and friendship have never waned.
He will be greatly missed by all who had the privilege of knowing him as a mathematician colleague and dear friend.
