Roger Heath-Brown writes:
Alan Baker, who was
elected a member of
the London Mathematical Society on 4th April
1966, died on 4th February 2018 following a severe stroke. He was one
of the most prominent
British number theorists
of his generation, and
the founder of modern transcendental number theory.
Alan was born in Finsbury, London, on 19th August
1939, the only child of Barnet and Bessie Baker (née
Sohn). After studying at Stratford Grammar School he
won a state scholarship to do a degree in mathematics at UCL. He then moved to Cambridge, completing
a PhD in 1964 under Harold Davenport, with a thesis
entitled “Some Aspects of Diophantine Approximation". In 1965 he was elected a Fellow of Trinity College
Cambridge, where he was based for the rest of his
career. He was awarded a Fields Medal at the 1970
ICM in Nice, became a Fellow of the Royal Society in
1973, and was appointed to a chair by the University
of Cambridge in 1974. He was fond of remarking on
the unusual chronological order of these events!
Baker had 8 PhD students: Coates, Odoni, Masser,
Stewart, Flicker, Heath-Brown, Mason, and Coleman.
Of these, only Masser and Stewart ended up working in the same area as Baker, the others pursuing
dierent interests.
Baker’s most important work concerns “linear forms
in logarithms”. For example, if α1; : : :; αn, β1; : : :; βn
are algebraic numbers, then Baker showed that the
linear form
σ :=
nXi=1
βi log αi
must be non-vanishing, except in the obvious trivial
cases. Moreover, and crucially, he gave an explicit
positive lower bound for jσj. This produced a whole
new class of transcendental numbers, a very simple example being π + log 2. His work also had very
important consequences for Diophantine equations.
For example, if F (x; y) 2 Z[x; y] is a binary form of
degree at least 3, it had been known since the work
of Thue (1909) that the equation F (x; y) = 1 has
nitely many solutions. However the unusual logical
structure of Thue’s argument meant that it was not
necessarily possible to nd all the solutions. In contrast, Baker’s work led to an explicit bound BF such
that any solution must lie in the box max(jxj; jyj) ≤
BF . Although the bound BF produced by the method
is very large, Baker and his followers were able to
make the techniques viable for practical examples.
Thus, in joint work with Davenport, he showed that
there is only one positive integer N for which each
of N + 1; 3N + 1 and 8N + 1 is a square, namely
N = 120.
Baker produced a urry of important papers up to
1980, by which time his work had led to the new eld
of transcendental number theory, with a worldwide
following. This is evidenced by the MSC classication
11J86 “Linear forms in logarithms; Baker’s method".
His treatise Transcendental number theory (CUP, 1975)
gives a comprehensive account of the early developments. Subsequently he was less involved in the
subject, but returned to it in 2007 with the book Logarithmic forms and Diophantine geometry (CUP, 2007),
written jointly with Wustholz. Baker also produced
two number theory textbooks, A concise introduction
to the theory of numbers (CUP, 1984) and A comprehensive course in number theory (CUP, 2012). Overall
it is clear that Baker’s work made a contribution of
enduring signicance to the development of mathematics.
Béla Bollobás writes: In spite of his great mathematical achievements, Baker was not often happy: he
saw himself as snubbed by what he viewed as the
mathematical establishment because he was born
‘on the wrong side of the tracks’. He spent most of
his time in Trinity College, with now and then a trip
to London. He had some outstanding research students, and he remained proud of what they achieved.
He liked to go for long walks, and was a keen player
of Trinity Bowls.
He loved his occasional trips abroad, to the States,
China and various European countries; in particular,
he was happy to collaborate with Gisbert Wüstholz
at ETH, and he enjoyed being pampered by Vera T.
Sós and Kálmán Gyo´´ry in Hungary.
He was a very private person, but in the company
of his friends he was talkative and lively: he was a
generous dinner guest, who happily contributed to
the conversation, and he had an unexpected penchant for dancing. Sadly, though, as his hearing failed
towards the end of his life, he slowly became more
reclusive.
